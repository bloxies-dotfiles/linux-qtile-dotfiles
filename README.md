My Linux QTile Dotfiles. Managed with YADM

Resistance is QTile!

Work In Progress
Fonts will have to be installed manually, Icons/Themes are included

[The Theme Used - Ultimate Dark Red](https://www.gnome-look.org/p/1246387/)

[The Icons Used - Ultimate Maia Red](https://www.opendesktop.org/p/1218961/)


Required Things : Qtile Wpgtk Pywal [Compton-Tryone](https://github.com/tryone144/compton) Dunst Rofi Pulseaudio Noto-fonts Noto-fonts-emoji Noto-fonts-cjk Noto-fonts-tc

My Personal System Things : Xorg NetworkManager Network-Manager-Applet Lightdm Slick-greeter Lightdm-gtk-greeter-settings Zsh Udisks2 Udiskie Wine W3M

My Personal Applications : Chromium Firefox Thunar Deluge PlayOnLinux Geany Xarchiver Rxvt-unicode-pixbuf HTop Feh Neofetch Vim Vlc Spotify Steam [Discord](https://github.com/Bios-Marcel/cordless) Gimp Flameshot Lxappearance Gpick Pavucontrol
