#!/bin/sh
setxkbmap gb
xsetroot -cursor_name left_ptr
udiskie &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
wal -i "/home/bloxiebird/.wallpapers/wallpaper.jpg" &
nm-applet &
dunst &
compton &
pulseaudio --start &
startprogramsrofi
