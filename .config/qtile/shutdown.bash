#!/bin/bash

[ $(echo -e "Yes\nNo" | rofi -dmenu -i -p "Are you sure that you want to exit QTile?") \= "Yes" ] && qtile-cmd -o cmd -f shutdown &

