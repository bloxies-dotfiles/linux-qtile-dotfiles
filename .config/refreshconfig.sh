#!/bin/sh

sudo rm -r /usr/share/icons/*
sudo cp -r ~/.icons/* /usr/share/icons/

sudo rm -r /usr/share/themes/*
sudo cp -r ~/.themes/* /usr/share/themes/

sudo rm -r /usr/share/backgrounds/*
sudo cp -r ~/.wallpapers/* /usr/share/backgrounds/

sudo rm -r /bin/shutdownrofi
sudo cp ~/.config/qtile/shutdown.bash /bin/shutdownrofi

sudo rm -r /bin/startprogramsrofi
sudo cp ~/.config/qtile/startprogramsrofi.bash /bin/startprogramsrofi
